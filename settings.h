//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author:	ADEW
// Version:	1.00
// Date:	30/11/2002
//
// Copyright ADEW 2002.  All rights reserved.
// No part of this code (either wholly or partially) maybe copied, passed on, published or sold to any person or persons without the written consent of ADEW.
//
// For help, visit http://www.adew.co.uk/nt_services/help
//

#ifndef _SETTINGS_H
#define _SETTINGS_H

// *********************************
// Settings
//
// These settings define the name used to define the service in the service Manager etc.
// *********************************

// name of the executable
#define SZAPPNAME            "NtServiceCode.exe"

// internal name of the service
#define SZSERVICENAME        "ADEW NtServiceCode"

// displayed name of the service
#define SZSERVICEDISPLAYNAME "ADEW NtServiceCode"

// list of service dependencies - "dep1\0dep2\0\0"
#define SZDEPENDENCIES       ""
//////////////////////////////////////////////////////////////////////////////

#endif