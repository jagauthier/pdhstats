#include <windows.h>
#include <stdio.h>

#include "main.h"
#include "service.h"


void ErrorDisp(char * lpszFunction, DWORD dw, int fd)
{
    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );

    cprintf(fd, "%s failed with error %d: %s\n",
        lpszFunction, dw, lpMsgBuf);
    LocalFree(lpMsgBuf);
}

