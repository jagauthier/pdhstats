#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdarg.h>
#include <malloc.h>
#include <string.h>
#include <windef.h>
#include <winbase.h>
#include <winsock.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "main.h"
#include "service.h"


void my_perror(char *error)
{
#ifndef WIN32
   perror(error);
#else
   log("%s: %i", error, WSAGetLastError());
   fprintf(stderr, "%s: %i\n", error, WSAGetLastError());
#endif
}

PdhQuery pq;
int port, number_cpus, network_ifs, disk_count;
char net_cards[10][256]; // 10 cards, no more
char diskdrives[26][5]; // one per alphabet letter
char version[16];
bool service;
bool analyze;
bool console;
bool skipnet;
bool DEBUG;
DWORD dwErr = 0;

#define NETLOOP 10

int vsocket;
bool servicestop = FALSE;

void usage(char *name){
   printf( "%s -install		to  the service\n", name );
   printf( "%s -remove		to remove the service\n", name );
   printf( "%s -console 	to run as a console app for debugging\n", name);
   printf( "%s -analyze		to give some hardware info\n", name);
   printf( "%s -version		to  retrieve the version\n", name );
}

BOOL GetAppVersion( char *LibName, WORD *MajorVersion, WORD *MinorVersion, WORD *BuildNumber, WORD *RevisionNumber )
{
 DWORD dwHandle, dwLen;
 UINT BufLen;
 LPTSTR lpData;
 VS_FIXEDFILEINFO *pFileInfo;
 dwLen = GetFileVersionInfoSize( LibName, &dwHandle );
 if (!dwLen) 
  return FALSE;

 lpData = (LPTSTR) malloc (dwLen);
 if (!lpData) 
  return FALSE;

 if( !GetFileVersionInfo( LibName, dwHandle, dwLen, lpData ) )
 {
  free (lpData);
  return FALSE;
 }
 if( VerQueryValue( lpData, "\\", (LPVOID *) &pFileInfo, (PUINT)&BufLen ) ) 
 {
  *MajorVersion = HIWORD(pFileInfo->dwFileVersionMS);
  *MinorVersion = LOWORD(pFileInfo->dwFileVersionMS);
  *BuildNumber = HIWORD(pFileInfo->dwFileVersionLS);
  *RevisionNumber = LOWORD(pFileInfo->dwFileVersionLS);
  free (lpData);
  return TRUE;
 }
 free (lpData);
 return FALSE;
}

void get_version()
{
    char ownPth[MAX_PATH]; 
	WORD MajorVersion        =0; 
	WORD MinorVersion        =0; 
	WORD BuildNumber        =0; 
	WORD RevisionNumber        =0;

    // Will contain exe path
    HMODULE hModule = GetModuleHandle(NULL);
    if (hModule != NULL)
    {
     // When passing NULL to GetModuleHandle, it returns handle of exe itself
        GetModuleFileName(hModule,ownPth, (sizeof(ownPth))); 
		GetAppVersion(ownPth,
              &MajorVersion,    
              &MinorVersion,    
              &BuildNumber,    
              &RevisionNumber);
	     sprintf(version, "%i.%i.%i.%i", MajorVersion, MinorVersion,BuildNumber,RevisionNumber);
    }
    else
    {
     sprintf(version, "No version");
    }
    
}
/* this function is copied from MS examples */
#define OBJECT_LIST_SIZE 4096

void enumerate(const char *text)
{
    PDH_STATUS  pdhStatus               = ERROR_SUCCESS;
    LPTSTR      szCounterListBuffer     = NULL;
    DWORD       dwCounterListSize       = 0;
    LPTSTR      szInstanceListBuffer    = NULL;
    DWORD       dwInstanceListSize      = 0;
    LPTSTR      szThisInstance          = NULL;
    int i, retval, data;
    char recv[128], send[128];
    long long r, s;
	DWORD dwCharsNeeded = OBJECT_LIST_SIZE;
	TCHAR mszObjects[OBJECT_LIST_SIZE];
	
    /* Because this is called multiple times */    
    disk_count=0;
	
	PdhEnumObjects(
		NULL, NULL, mszObjects, &dwCharsNeeded, PERF_DETAIL_WIZARD, TRUE);
	
	
    pdhStatus = PdhEnumObjectItems (
        NULL,                   // reserved
        NULL,                   // local machine
        TEXT(text),        // object to enumerate
        szCounterListBuffer,    // pass in NULL buffers
        &dwCounterListSize,     // 0 to get length
        szInstanceListBuffer,   // required size
        &dwInstanceListSize,    // of the buffers in chars
        PERF_DETAIL_WIZARD,     // counter detail level
        0);

    // Allocate the buffers and try the call again.
        szCounterListBuffer = (LPTSTR)malloc (
            (dwCounterListSize * sizeof (TCHAR)));
        szInstanceListBuffer = (LPTSTR)malloc (
            (dwInstanceListSize * sizeof (TCHAR)));

        if ((szCounterListBuffer != NULL) &&
            (szInstanceListBuffer != NULL))
        {
            pdhStatus = PdhEnumObjectItems (
                NULL,   // reserved
                NULL,   // local machine
                TEXT(text), // object to enumerate
                szCounterListBuffer,    // passing in NULL buffers
                &dwCounterListSize,     // passing in 0
                szInstanceListBuffer,
                &dwInstanceListSize,
                PERF_DETAIL_WIZARD,     // counter detail level
                0);
            if (pdhStatus == ERROR_SUCCESS)
            {

            // Walk the return instance list.
                for (szThisInstance = szInstanceListBuffer;
                     *szThisInstance != 0;
                     szThisInstance += lstrlen(szThisInstance) + 1)
                {
                  if (!strcmp(text, "Processor"))
                   number_cpus++;
                else
                  if (!strcmp(text, "Network Interface")) {
                  	/* skip the stupid interfaces, we want phyiscal ones */
                     if (!strstr(szThisInstance, "oopback") && !strstr(szThisInstance, "isatap") && !strstr(szThisInstance, "Teredo") && !strstr(szThisInstance, "Miniport"))   {
                     	r=0;
                     	s=0;
                     	data=0;
                     	/* This will make start up take longer, but for some reason there are NICs showing up that the OS doesn't even see?
                     	So, we'll poll a few times, and if there's not any data, we'll just skip it. */
                     	
                        sprintf(recv, "\\Network Interface(%s)\\Bytes Received/sec", szThisInstance);
      					sprintf(send, "\\Network Interface(%s)\\Bytes Sent/sec", szThisInstance);
      
						retval=pq.addCounter(recv);
      					retval=pq.addCounter(send);
      					
      					for (i=0; i<NETLOOP; i++) {
      						log (" Testing: %s", szThisInstance);
      						pq.updateCounters();
				        	r = pq.getCounter(recv);
       						s = pq.getCounter(send);
                     	
                     		if (r>0 || s>0) {
                     			log("  %s has data. Adding.", szThisInstance);
	                       		strcpy(net_cards[network_ifs], szThisInstance);
    	                   		network_ifs++;
    	                   		data=1;
    	                   		break; // leave the testing loop
    	               	    }
    	               	    if (!skipnet)
    	               	    	sleep(1);
                        } 
						if (!data)
						 	log("  %s doesn't have data. Skipping.", szThisInstance);
                  }
               }
				else 
				  if (!strcmp(text, "LogicalDisk")){
				  	if (strstr(szThisInstance, ":")) {
				  		log("Found drive: '%s'", szThisInstance);
				  		strcpy(diskdrives[disk_count], szThisInstance);
				  		disk_count++;
				  }
				  }
                }
           }
           else {
           	log("Could not enumerate %s", text);
           }
        }
        else
        {
              log ("Enumerate: unable to allocate buffers");
        }

        if (szCounterListBuffer != NULL)
            free (szCounterListBuffer);

        if (szInstanceListBuffer != NULL)
            free (szInstanceListBuffer);


    // We do this to get rid of the _Total counter
    if (!strcmp(text, "Processor"))  {
       number_cpus--;
         log("Processors found: %i", number_cpus);
       }
     else
     if (!strcmp(text, "Network Interface"))  {
        log("Network interfaces found: %i", network_ifs);
       for (i=0; i<network_ifs; i++){
          log("  %s", net_cards[i]);
        }
    }
	else 
	if (!strcmp(text, "LogicalDisk"))  {
	    log("Disks found: %i", disk_count);
       	for (i=0; i<disk_count; i++){
          log("  %s", diskdrives[i]);
        }
    }
		
	
    return;
}

void go(void) {
   int retval;
   
   DEBUG=1;
   
   get_version();
   retval=pq.init();

   if (!retval) {
	 log("pq.init(): %s",pq.getLastErrorString());
	 exit(1);
   }
   // Count the processors
   enumerate("Processor");
   enumerate("Network Interface");
   enumerate("LogicalDisk");
   if (analyze) exit(0);

   if (!console) {
    log("Starting major counters.");
    
      process((char *)"DISKS", BUNKFD);
      process((char *)"GETCPU", BUNKFD);
      process((char *)"CPUSTATS", BUNKFD);
      process((char *)"UPTIME", BUNKFD);
      process((char *)"NET", BUNKFD);
      process((char *)"MEMSTATS", BUNKFD);
    }
	log ("Version '%s' ready.", version);
   DEBUG=0;
   vsocket = initialize();
   main_loop(vsocket);

   return;
}

void parse_command_line(int argc, char **argv)
{
  int i;

  for (i=0; i<argc; i++) {
   if ( (argc > 1) &&
		((*argv[i] == '-') || (*argv[i] == '/')) )
	 {
        if ( _stricmp( "install", argv[i]+1 ) == 0 )
		  {
			 CmdInstallService();
             exit(0);
		  }
        else if ( _stricmp( "remove", argv[i]+1 ) == 0 )
		  {
			 CmdRemoveService();
             exit(0);
		  }
        else if ( _stricmp( "analyze", argv[i]+1 ) == 0 )
		  {
		     analyze=TRUE;
		     console=TRUE;
			 return;
		  }
        else if ( _stricmp( "console", argv[i]+1 ) == 0 )
		  {
		     console=TRUE;
			 return;
		  }
		   else if ( _stricmp( "skipnet", argv[i]+1 ) == 0 )
		  {
		     skipnet=TRUE;
		     console=TRUE;
			 return;
		  }
  		else if ( _stricmp( "version", argv[i]+1 ) == 0 ) {
  			get_version();
        	printf("%s\n", version);
       		return;
       	}
   		else if ( _stricmp( "port", argv[i]+1 ) == 0 )
		  {
             if (strlen(argv[i+1])==0)
               port = 5678;
             else
			 port=atoi(argv[i+1]);
			 if (port==0) port=5678;
              i++;
			 if (port>65535 || port<1024)
			   log("Invalid port: %i.", port);
		  }
        else
		  {
			 usage(argv[0]);
		  }
	 }
  }

}

int main(int argc, char **argv)
{
    SERVICE_TABLE_ENTRY dispatchTable[] =
    {
        { TEXT((char *)SZSERVICENAME), (LPSERVICE_MAIN_FUNCTION)service_main },
        { NULL, NULL }
    };
     log("Performance counter service starting.");

    port=5678;

   parse_command_line(argc, argv);

   if (!console) {
      StartServiceCtrlDispatcher(dispatchTable);
      cleanup();

      }
 else
   go();

}

char *printdate(void)
{
   struct tm *tp;
   static char  date[80];
   time_t thetime = time(0);
   tp = localtime(&thetime);

   sprintf(date, (char *)"%d/%d/%02d %d:%02d:%02d", 1 + tp->tm_mon, tp->tm_mday,
           tp->tm_year % 100, tp->tm_hour, tp->tm_min,tp->tm_sec);
   return date;
}

void log(const char *fmt, ...)
{
	char buf[4096];
    struct stat st;
    FILE *fp;
    int size;
	va_list args;
	
	if (!DEBUG) return;
	
	va_start(args, fmt);
	vsprintf(buf, fmt, args);
	va_end(args);

    stat(DEBUGFILE, &st);
    size = st.st_size;
    if (size>MAXLOGSIZE){
      fp=fopen(DEBUGFILE, "w");
      fprintf(fp, "%s: Log file truncated. Exceeded %i bytes.\n", printdate(), size);
      fclose(fp);
    }

    fp=fopen(DEBUGFILE, "a");
    fprintf(fp, "%s: %s\n", printdate(), buf);
    fclose(fp);

    if (console) {
	 printf("%s: %s\n", printdate(), buf);
	 }
    else {
    /* FIXME -- Implement event logging */
    /* AddToMessageLog(buf, type); */
	  }

}

#define EVMSG_SVCCTL                     ((DWORD)0x20000090L)

VOID AddToMessageLog(LPTSTR lpszMsg, LPTSTR lpszType)
{
    TCHAR   szMsg[256];
    HANDLE  hEventSource;
    LPTSTR  lpszStrings[2];
	WORD	wEventType;
    return;
		if (!strcmp("ERROR", lpszType))
			wEventType=EVENTLOG_ERROR_TYPE;

		if (!strcmp("WARNING", lpszType))
			wEventType=EVENTLOG_WARNING_TYPE;

		if (!strcmp("INFORMATION", lpszType))
			wEventType=EVENTLOG_INFORMATION_TYPE;

		if (!strcmp("AUDIT_SUCCESS", lpszType))
			wEventType=EVENTLOG_AUDIT_SUCCESS;

		if (!strcmp("AUDIT_FAILURE", lpszType))
			wEventType=EVENTLOG_AUDIT_FAILURE;

         dwErr = GetLastError();
        // Use event logging to log the error.
        //
        hEventSource = RegisterEventSource(NULL, TEXT(SZSERVICENAME));

        sprintf(szMsg, "%s: ", TEXT(SZSERVICENAME));
        lpszStrings[0] = szMsg;
        lpszStrings[1] = lpszMsg;

        if (hEventSource != NULL) {
            ReportEvent(hEventSource, // handle of event source
                wEventType,			  // event type
                0,                    // event category
                EVMSG_SVCCTL,                    // event ID
                NULL,                 // current user's SID
                2,                    // strings in lpszStrings
                0,                    // no bytes of raw data
                (const char **)lpszStrings,          // array of error strings
                NULL);                // no raw data

            (VOID) DeregisterEventSource(hEventSource);
        }
}

/* a function, because WIN32 and unix closes are not interoperable */
void disconnect(int vsocket)
{
#ifdef WIN32
   closesocket(vsocket);
#else
   close(vsocket);
#endif

}

void cleanup(void)
{
   disconnect(vsocket);
#ifdef WIN32
   /* Shut down Windows sockets */
   WSACleanup();                 /* clean up */
#endif
   log("Shut down successfully.");
}

