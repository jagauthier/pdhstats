#define _WIN32_WINNT 0x0500

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdarg.h>
#include <malloc.h>
#include <string.h>
#include <windef.h>
#include <winbase.h>
#include <winsock.h>
#include <windows.h>
#include <tchar.h>

#include "main.h"

#define CPULOAD "\\Processor(_Total)\\% Processor Time"
#define UPTIME   "\\System\\System Up Time"
#define CPUQUEUE "\\System\\Processor Queue Length"

#define AVAILMEM "\\Memory\\Available Bytes"
#define WORKINGSET "\\Process(_Total)\\Working Set"
#define CACHE  "\\Memory\\Cache Bytes"
#define NONPAGE "\\Memory\\Pool Nonpaged Bytes"

#define PAGES "\\Memory\\Pages/sec"
#define PAGESIN "\\Memory\\Pages Input/sec"
#define PAGESOUT "\\Memory\\Pages Output/sec"
#define PAGEREADS "\\Memory\\Page Reads/sec"



MEMORYSTATUSEX mem_statusex;

extern int network_ifs, number_cpus, disk_count;
extern char net_cards[10][256];
extern char diskdrives[26][5];
extern char version[16];

void process(char *pdhstring, int fd)
{
   char uptime[255], buf[255];
   char netcounter[255];
   int cpuload, cpuqueue;
   int days, hours, minutes, seconds;
   int retval;
   DWORD dw;
   char counter[1024];
   
    if (strlen(pdhstring)<3) {
       log("Invalid string to process. returning 255.");
       cprintf(fd, "255\n");
       return;
    }

   strcpy(counter, pdhstring);

   log("Request: %s", counter);

   if (!strcmp(pdhstring, "VERSION")) {
       cprintf(fd, "%s\n", version);
       return;
   }

   if (!strcmp(pdhstring, "MEMORY")) {
     long long mtot, mavail, ptot, pavail;
     long long mperc, pperc;
	 char mem[12], pag[12], memout[12];
     mem_statusex.dwLength = sizeof (mem_statusex);
     GlobalMemoryStatusEx(&mem_statusex);
     dw = GetLastError();

     mtot = mem_statusex.ullTotalPhys/1024;
     log ("mem: %u", mtot);
     mavail = (mem_statusex.ullAvailPhys)/1024*100;
     log("ava: %u", mavail);
     mperc = 100-(mavail/mtot);
     sprintf(mem, "%u\n", mperc);
     log ("per: %u", mperc);
     ptot = (mem_statusex.ullTotalPageFile)/1024;
     log ("pag: %u", ptot);
     pavail = (mem_statusex.ullAvailPageFile)/1024*100;
     log("ava: %u", pavail);
     pperc = 100-(pavail/ptot);
     sprintf(pag, "%u\n", pperc);
     log ("per: %u", pperc);
    
	 cprintf(fd, "%s%s", mem, pag);
     return;
   }
  if (!strcmp(pdhstring, "MEMSTATS")) {
  	
  	char memall[1024], memwork[128];
  	
  	unsigned long long avail=0, working=0, cache=0, nonpage=0;
  	int pages, pagesin, pagesout, pagereads;
  	
  	 strcpy(memall, "Avail,Working,Cache,NonPaged,pages/sec,pages in/sec,pages out/sec,page read/sec\n");
  	 
  	 pq.addCounter((char*)AVAILMEM);
  	 pq.addCounter((char*)WORKINGSET);
  	 pq.addCounter((char*)CACHE);
  	 pq.addCounter((char*)NONPAGE);
  	
  	  	 
  	 pq.addCounter((char*)PAGES);
  	 pq.addCounter((char*)PAGESIN);
  	 pq.addCounter((char*)PAGESOUT);
  	 pq.addCounter((char*)PAGEREADS);
  	 
  	 avail=pq.getCounter((char*)AVAILMEM);
  	 working=pq.getCounter((char*)WORKINGSET);
 	 cache=pq.getCounter((char*)CACHE);
  	 nonpage=pq.getCounter((char*)NONPAGE);

  	 pages=pq.getCounter((char*)PAGES);
  	 pagesin=pq.getCounter((char*)PAGESIN);
  	 pagesout=pq.getCounter((char*)PAGESOUT);
  	 pagereads=pq.getCounter((char*)PAGEREADS);


	 sprintf(memwork, "%llu,%llu,%llu,%llu,%i,%i,%i,%i\n", avail,working,cache,nonpage,pages,pagesin,pagesout,pagereads);
     strcat(memall, memwork);
     
	 cprintf(fd, "%s", memall);
     return;
   }
   else
   if (!strncmp(pdhstring, "NET", 3)) {
      int netc=0;
      char neteach[2048], netall[2048];
      char recv[512], send[512], band[512];;
      long long r, s, bw;
      double rcv_perc, snd_perc;
      int net_loop=0;
      netcounter[0]=0;
      netall[0]=0;

      strcpy(netall, "NIC,link speed,rcv/sec,snd/sec,rcv %,snd %\n");
      for (net_loop=0; net_loop<network_ifs; net_loop++) {
		log("Processing: %s", net_cards[net_loop]);
   /*   for (netc=4; pdhstring[netc]!=0; netc++)
       netcounter[netc-4]=pdhstring[netc];
      netcounter[netc-4]=0;*/
      sprintf(recv, "\\Network Interface(%s)\\Bytes Received/sec", net_cards[net_loop]);
      sprintf(send, "\\Network Interface(%s)\\Bytes Sent/sec", net_cards[net_loop]);
      sprintf(band, "\\Network Interface(%s)\\Current Bandwidth", net_cards[net_loop]);

      pq.addCounter(recv);
      pq.addCounter(send);
      pq.addCounter(band);
  
	  //  Updates automatically when select() times out after 5 seconds.
      //  pq.updateCounters();

       r = pq.getCounter(recv);
       s = pq.getCounter(send);
 	   bw = pq.getCounter(band);
 	   /* Hack :( 
 	   Hyper-V Nics show an unreasonable large number that only fits into a 64b space.
 	   So, this just forces anything that overflows to 1G speed. */
 	   if (bw < 0) bw = 1000000000;

      /* continue if we get an error results from this NIC */
       if (pq.last_error!=ERROR_SUCCESS) {
         continue;
         }

		// if (r>0 && s>0) {
		   rcv_perc=((double)r/(double)bw)*100;
		   snd_perc=((double)s/(double)bw)*100;
    	   sprintf(neteach, "%s,%lld,%lld,%lld,%.0f,%.0f\n", net_cards[net_loop],bw,r,s,rcv_perc,snd_perc);
    	   strcat(netall, neteach);
   		//}
         
		}
	    cprintf(fd, "%s", netall);
       return;
      }

   else
   if (!strcmp(pdhstring, "GETCPU")) {
      int cpu, cputemp;
      char cpustr[128], cpueach[128], cpuall[128];
      cpuall[0]=0;
      for (cpu=0; cpu<number_cpus;cpu++) {
       sprintf(cpustr, "\\Processor(%i)\\%% Processor Time", cpu);

       pq.addCounter(cpustr);

	   //  Updates automatically when select() times out after 5 seconds.
       // pq.updateCounters();

       cputemp = pq.getCounter(cpustr);
       sprintf(cpueach, "%i\n", cputemp);
       strcat(cpuall, cpueach);
       }
       cprintf(fd, "%s", cpuall);
       return;
   }
   else
	if (!strcmp(pdhstring, "PROCS")) {
       cprintf(fd, "%i\n", number_cpus);
       return;
   }
  else
  if (!strcmp(pdhstring, "CPUTOTAL")) {
      retval= pq.addCounter((char *)CPULOAD);
         //  Updates automatically when select() times out after 5 seconds.
		 //pq.updateCounters();
      retval = pq.getCounter((char *)CPULOAD);
	  cprintf(fd, "%i\n", retval);
 	  return;
   }
   else
     if (!strcmp(pdhstring, "CPUSTATS")) {
         pq.addCounter((char *)CPULOAD);
         pq.addCounter((char *)CPUQUEUE);
         //  Updates automatically when select() times out after 5 seconds.
		 //pq.updateCounters();
	     cpuload = pq.getCounter((char *)CPULOAD);
	     cpuqueue = pq.getCounter((char *)CPUQUEUE);
		 cprintf(fd, "CPU Load,CPU Queue,CPU Count\n%i,%i,%i\n", cpuload, cpuqueue, number_cpus);
 	  return;
   }
   
   
   else
   if (!strcmp(pdhstring, "UPTIME")){

	  uptime[0]=0; /* so we can strcat */
      pq.addCounter((char *)UPTIME);
         //  Updates automatically when select() times out after 5 seconds.
         // pq.updateCounters();
      pq.getCounter((char *)UPTIME);
    
	  days = (retval/3600)/24;
	  hours = (retval - days * 24 *3600) / 3600;
	  minutes = (retval/60)%60;
	  seconds = (retval)%60;

	  if (days>0) {
		  sprintf(buf, "%i day%s", days, (days==1)?"":"s");
		  strcat(uptime, buf);
		  if (hours|| minutes || seconds)
		    strcat(uptime, ", ");
	  }
	  if (hours>0)  {
		  sprintf(buf, "%i hour%s", hours, (hours==1)?"":"s");
		  strcat(uptime, buf);
	      if (minutes>0 || seconds>0)
		    strcat(uptime, ", ");
	  }
	  if (minutes>0)  {
		  sprintf(buf, "%i minute%s", minutes, (minutes==1)?"":"s");
		  strcat(uptime, buf);
	      if (seconds>0)
		    strcat(uptime, ", ");
	  }

	  if (seconds>0) {
		  sprintf(buf, "%i second%s", seconds, (seconds==1)?"":"s");
		  strcat(uptime, buf);
	  }

	  	  strcat(uptime, ".\n");
	  	  cprintf(fd, uptime);
	  	  return;
  }
     else
     /* 06/03/15 - Added this to return multiple disk informations. */
   if (!strcmp(pdhstring, "DISKS")){
   		int disk_loop, diskqueue_i, diskread_i, diskwrite_i;
   		char diskqueue[128], diskread[128], diskwrite[128];
   		char diskline[128], diskout[2048], drive[5];
   		unsigned __int64 i64FreeBytesToCaller, i64TotalBytes,  i64FreeBytes;
		int perc;

		strcpy(diskout, "Disk,Total,Free,% Free,Queue,% ReadTime,% WriteTime\n");

   	    for (disk_loop=0; disk_loop<disk_count; disk_loop++) {
			log("Processing: %s", diskdrives[disk_loop]);
				sprintf(diskqueue, "\\LogicalDisk(%s)\\Current Disk Queue Length", diskdrives[disk_loop]);
				sprintf(diskread,  "\\LogicalDisk(%s)\\%% Disk Read Time", diskdrives[disk_loop]);
				sprintf(diskwrite, "\\LogicalDisk(%s)\\%% Disk Write Time", diskdrives[disk_loop]);
				
         		pq.addCounter(diskqueue);
         		pq.addCounter(diskread);
         		pq.addCounter(diskwrite);
         		
 				diskqueue_i = pq.getCounter(diskqueue);
 				diskread_i = pq.getCounter(diskread);
 				if (pq.last_error!=ERROR_SUCCESS) {
     				log("Error: %s", pq.getLastErrorString());
				 	return;
  		 		}	
 				
 				diskwrite_i = pq.getCounter(diskwrite);
 				
 				strcpy(drive, diskdrives[disk_loop]);
 				strcat(drive, "\\");

        	if(GetDiskFreeSpaceEx(drive,(PULARGE_INTEGER)&i64FreeBytesToCaller,(PULARGE_INTEGER)&i64TotalBytes, (PULARGE_INTEGER)&i64FreeBytes)){
				i64FreeBytesToCaller /= (1024*1024);
	        	i64TotalBytes /= (1024*1024);
	        	perc = (int)(i64FreeBytesToCaller*100)/(int)i64TotalBytes;
	        	
		        sprintf(diskline, "%s,%I64u,%I64u,%i,%i,%i,%i\n", diskdrives[disk_loop],i64TotalBytes,i64FreeBytesToCaller,perc,diskqueue_i,diskread_i,diskwrite_i);
			}	
		strcat(diskout, diskline);	
		}
		
		cprintf(fd, "%s", diskout); 
		return;

	}
else {
    log("No matches, so it's a counter request.");
    retval=pq.addCounter(counter);
    
   	if (!retval && pq.last_error!=ERROR_SUCCESS) {
     cprintf(fd, "Querying '%s' returned an error: (%i)%s\n", counter, retval, pq.getLastErrorString());
	 return;
   	}

   retval = pq.getCounter(counter);

   log ("Returning: %i", retval);
   cprintf(fd, "%ld\n", retval);
}
   return;
}
