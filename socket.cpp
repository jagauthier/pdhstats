#include<stdlib.h>

#include<string.h>

#ifdef WIN32
#include <winsock2.h>
#include <io.h>
#include <direct.h>
#else
#include <sys/time.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include<errno.h>
#include <ctype.h>
#include<stdio.h>
#include "main.h"
#include "service.h"

int get_request(int fd)
{
   char buf[1024];

   unsigned int index=0;

   for ( ; ; )
	 {
		int nRead;
		nRead = recv(fd,  buf + index, READSIZE, 0);
		if ( nRead > 0 )
		  {
			 index += nRead;
			 /* no one should be sending more than 1024 bytes to us */
			 if (index>sizeof(buf))
			   disconnect(fd);
		  }
		else if ( nRead == 0 )
		  {
			 break;
		  }
		else if ( errno == EWOULDBLOCK )
		  break;
		else
		  {
			 my_perror( (char *)"Read_from_descriptor" );
			 return FALSE;
		  }
		if (nRead < READSIZE) /*short read. buhbye */
		  break;

	 }
   buf[index]=0;

   if (buf[index-1]=='\n' ||
	   buf[index-1]=='\r')  {

      buf[index-1]=0;
	  index--;
	  }

   if (buf[index-1]=='\n'  ||
	    buf[index-1]=='\r')
      buf[index-1]=0;

/* log_message("INFORMATION", "Got: %s", buf);*/
   process(buf, fd); /* send it to be processed by the PDH stuff */
   return TRUE;
}


int new_connection(int socket)
{
   int new_desc;
   struct sockaddr_in sock;
   int size = sizeof (sock);
   char buf[128];

   getsockname(socket, (struct sockaddr *) &sock, &size);
   new_desc = accept(socket,(struct sockaddr *)&sock,&size);
   if (new_desc<0){
	  my_perror((char *)"accept");
	  exit(1);
   }

   if (getpeername (new_desc, (struct sockaddr *) &sock, &size) < 0)
	 {
		my_perror ((char *)"getpeername");
		strcpy(buf, "an unknown host");
	 }
   else
	 {
		int addr;

		addr = ntohl (sock.sin_addr.s_addr);
		sprintf (buf, "%d.%d.%d.%d",
				 (addr >> 24) & 0xFF, (addr >> 16) & 0xFF,
				 (addr >> 8) & 0xFF, (addr) & 0xFF
				 );
		strcpy(buf, inet_ntoa(sock.sin_addr));
	 }
   log("Connection from %s (%i)", buf, new_desc);
   return (new_desc);
}

int initialize(void)
{
   int create_socket;

   struct sockaddr_in address;


#if defined(WIN32)
	 {
		/* Initialise Windows sockets library */

		unsigned short wVersionRequested = MAKEWORD(2, 0);
		WSADATA wsadata;
		int err;

		err = WSAStartup(wVersionRequested, &wsadata);
		if (err)
		  {
			 log("Error %i on WSAStartup", err);
			 exit(1);
		  }
	 }
#endif
   create_socket = socket(AF_INET,SOCK_STREAM,0);
   if (create_socket==-1){
	  my_perror((char *)"socket");
	  exit(1);
   }

   address.sin_family = AF_INET;
   address.sin_addr.s_addr = INADDR_ANY;

   address.sin_port = htons(port);

   if (bind(create_socket,(struct sockaddr *)&address,sizeof(address))==-1){
	  my_perror((char *)"bind");
	  exit(1);
   }

   if (listen(create_socket,3)==-1){
	  my_perror((char *)"listen");
	  exit(1);
   }
   log("Listening on port %i. (%i)", port, create_socket);
   return create_socket;
}

void main_loop(unsigned int socket)
{
   int maxfd, i,j, fd;
   int numfds, diskCounter=0;
   unsigned int new_desc;
   bool out=true;
   struct timeval tv;
   fd_set in, temp;

   FD_ZERO(&in);
   FD_ZERO(&temp);
   FD_SET(socket, &in);
   FD_SET(socket, &temp);
   maxfd = socket;

   tv.tv_sec = 5;
   tv.tv_usec = 0;
	
   for (;;) {
   	   diskCounter++;
   	   if (diskCounter>=30) {
   	   	  enumerate("LogicalDisk");
   	   	  diskCounter=0;
   	   }
   	   pq.updateCounters();
       in = temp;
         for (i=0; i<maxfd+1; i++) {
         	  if (FD_ISSET(i, &temp)) {
           		if (out) {
             		log ("Watching on file descriptor: %i ", i);
             		out=false;
         		}	
         	}	
         }
     
	  numfds = select (maxfd + 1, &in, NULL, NULL, &tv);

	  if (numfds==-1){
		 my_perror((char *)"select");
		 exit(1);
	  }

      if (numfds == 0) continue;

      /* Find first connection with available input */
//	  fd = -1;
	  for (i = 0; i <= maxfd; i++) {
		 if (FD_ISSET(i, &in)) {
			fd = i;
			/* new connection, it's on the bound socket */
			if (fd == socket){
               new_desc = new_connection(socket);
			   FD_SET(new_desc, &temp);

			  if (maxfd < new_desc) maxfd = new_desc;
            } // fd == socket
			else {
			   get_request(fd);
			   FD_CLR(fd, &temp);
			   disconnect(fd);
			} // else
         }  // ISSET
	  } // for i
	  in = temp;
   } // for (;;)
}

void cprintf(int fd, const char *fmt, ...)
{
	char buf[2048];
	va_list args;
    int sbytes;
    
    /* This is so we can run process() internally, without outputting anything. */
    /* The point is to set up counters when the service start */
    
    if (fd==BUNKFD) return;
    
	va_start(args, fmt);
	vsprintf(buf, fmt, args);
	va_end(args);


   sbytes=send (fd, buf, strlen(buf), 0);
   
   if (sbytes==SOCKET_ERROR)
	 my_perror((char *)"send");

//	log("Bytes sent: %i\n", sbytes); 
}


