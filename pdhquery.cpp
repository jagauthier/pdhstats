/*
/*
     Written By     :     Paul Maker
     Date          :     29/10/2001
     File          :     PdhQuery.cpp
     Descriptget_
     ion     :     Wraps the NT Performance Data Helper
*/
#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include "main.h"
#include "PdhQuery.h"

#define QUERY 5


/* zero the object */
PdhQuery::PdhQuery()
{
     count = 0;
     names = 0;
     counters = 0;
     last_error = 0;
}

/* close the object down */
PdhQuery::~PdhQuery()
{
     /* free the names */

     for(int i=0;i<count;i++)
     {
          if(names[i]) free(names[i]);
     }

     if(names)free(names);

     /* free the counter array, this is not a pointer to pointer just a straight
     array */
     if(counters)free(counters);

     /* close the query handle */
     PdhCloseQuery(query_handle);
}

int PdhQuery::lookUpIndex(const char *name)
{
     int retval = -1;
     for(int i=0;i<count;i++)
     {
          if(names[i]) /* care */
          {
               if(!stricmp(names[i],name))
               {
                    retval = i;
                    break;
               }
          }
     }
     return retval;
}

bool PdhQuery::upSizeNames(char *name)
{
     bool retval = false;
     if(names = (char**)realloc(names,((count + 1) * sizeof(char*))))
     {
          names[count] = NULL;
          if(names[count] = strdup(name)) retval = true;
          else
          {
               downSizeNames();
               last_error = PDH_MEMORY_ALLOCATION_FAILURE;
          }
     }
     else last_error = PDH_MEMORY_ALLOCATION_FAILURE;
     return retval;
}

bool PdhQuery::upSizeCounters()
{
     bool retval = false;
     if(counters = (PDHCOUNTERSTRUCT*)realloc(counters,
					   ((count + 1) * sizeof(PDHCOUNTERSTRUCT))))
     {
          retval = true;
     }
     else last_error = PDH_MEMORY_ALLOCATION_FAILURE;
/*
   if (last_error==PDH_MEMORY_ALLOCATION_FAILURE){
	  exit(1);
   }*/
     return retval;
}

bool PdhQuery::downSizeNames()
{
     bool retval = true;
     if(names[count]) free(names[count]);
     names = (char**)realloc(names,(count * sizeof(char*)));
     return retval;
}

bool PdhQuery::downSizeCounters()
{
     bool retval = true;
     counters = (PDHCOUNTERSTRUCT*)realloc(counters,
                                                        (count * sizeof(PDHCOUNTERSTRUCT)));
     return retval;
}

/* init the object */
bool PdhQuery::init()
{
     bool retval = false;
     if((last_error = PdhOpenQuery(NULL,1,&query_handle)) == ERROR_SUCCESS)
     {
          retval = true;
     }
     return retval;
}

/* add a counter */
bool PdhQuery::addCounter(char *name)
{
     bool retval = false;

     // If the counter is already added, just fake it and leave
	 if (lookUpIndex(name)>-1) return false;
	 
     if(upSizeCounters())
     {

          last_error = PdhAddCounter(query_handle, name,
                     (DWORD)&counters[count],
                     &counters[count].hCounter);
		  if (last_error ==ERROR_SUCCESS)

          {
               if(upSizeNames(name))
               {
                    ++count;
                    retval = true;
               }
         }
          else downSizeCounters();
     }
     if (retval == TRUE) {
     	// If we've added a counter, let's update them now, so the client gets immediate feedback.
        log ("Added new counter: %s", name);
        counter_added=TRUE;
		updateCounters();
     }
     else {
     	log("Error adding '%s': %s", name, getLastErrorString());
     }
     return retval;
}

bool PdhQuery::removeCounter(char *name)
{

	int count;
	count = lookUpIndex(name);
	if (count==-1) return FALSE;


    if (PdhRemoveCounter(counters[count].hCounter) == ERROR_SUCCESS) {
       downSizeCounters();
	   return TRUE;
	   }
	return FALSE;
}

// 08/18/04 - JG
// If the counters have been updated within the last QUERY seconds, do NOT reprobe.
// Much less overhead.  UNLESS a new counter has been added.

bool PdhQuery::updateCounters()
{
     bool retval = false;
     time_t now = time(0);

    if ( (now - last_update >= QUERY) || counter_added) {
    	
        if((last_error = PdhCollectQueryData(query_handle)) == ERROR_SUCCESS) {
           retval = true;
           last_update=now;
           }
     }
     else {
        retval = true;
          }
     counter_added=FALSE;
     last_error == ERROR_SUCCESS;
     return retval;
}

unsigned long long PdhQuery::getCounter(char *name)
{
     int i =0;
     unsigned long long retval = -1;
     if((i = lookUpIndex(name)) > -1)
     {
          PDH_FMT_COUNTERVALUE pdhFormattedValue;
          if((last_error = PdhGetFormattedCounterValue(counters[i].hCounter,
                                                PDH_FMT_LARGE,
                                                NULL,
                                                &pdhFormattedValue)) == ERROR_SUCCESS)
          {
               retval = pdhFormattedValue.largeValue;
          }
     }
     return retval;
}

char *PdhQuery::getLastErrorString()
{
     switch(last_error)
     {
     case ERROR_SUCCESS:
          strcpy(last_error_str,"There is no error");
          break;
     case PDH_INVALID_ARGUMENT:
          strcpy(last_error_str,"Invalid argument");
          break;
     case PDH_MEMORY_ALLOCATION_FAILURE:
          strcpy(last_error_str,"Memory allocation failure");
          break;
     case PDH_CSTATUS_BAD_COUNTERNAME:
          strcpy(last_error_str,"The counter name path string could not be parsed "
                                     "or interpreted");
          break;
     case PDH_CSTATUS_NO_COUNTER:
          strcpy(last_error_str,"The specified counter was not found");
          break;
     case PDH_CSTATUS_NO_COUNTERNAME:
          strcpy(last_error_str,"An empty counter name path string was passed in");
          break;
     case PDH_CSTATUS_NO_MACHINE:
          strcpy(last_error_str,"A machine entry could not be created");
          break;
     case PDH_CSTATUS_NO_OBJECT:
          strcpy(last_error_str,"The specified object could not be found");
          break;
     case PDH_FUNCTION_NOT_FOUND:
          strcpy(last_error_str,"The calculation function for this counter could "
                                     "not be determined");
          break;
     case PDH_INVALID_HANDLE:
          strcpy(last_error_str,"The query handle is not valid");
          break;
     case PDH_NO_DATA:
          strcpy(last_error_str,"No data for that counter");
          break;
     }
     return last_error_str;
}


