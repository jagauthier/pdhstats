This tool was developed in 2007.

It listens on a TCP port (5678 by default) and accepts performance counter paths.
It then looks up those counters, and returns the data to the client.
It was developed as an alternative to installing SNMP on a server, which at the time was less secure than present implementations.

It seems to only compile with BloodShed C++, the latest version (5.0 beta)
It does not compile with the successor, WxDev, so there's a binary included in this folder.
I've attempted to use newer versions if MinGW, but they also do not execute.

So, it can be built as-is, until someone decided to make this work with Visual Studio or something. I have no motiviation to try.
It caches performance monitor statistics for 5 minutes to reduce CPU load.


It has some built in calls:
VERSION:	Last date and time of compile
MEMORY:		Returns percentage of free RAM and Pagefile
NET:		Returns Bytes Sent and Received/sec for each NIC.  Not much purpose to this since
			there are so many NICs and you cannot accurately, dynamically graph them
DCPU:		Obsolete. Returned 2 CPUs. This was written back when that was. amazing.
GETCPU:		Returns load percentage of all CPUs individually
CPUTOTAL:	Returns the average of all the CPUS.
CPU:		Returns the same as above, with a label.
UPTIME:		Returns the uptime 

BUGS:
There is one known bug.  If the service enumerates 16-ish network adapters, it fails to start.
The workaround is to disable the network adapters that are not used, and reboot.
It will then enumerate much less, and succeed.  I don't know what the breaking point is.