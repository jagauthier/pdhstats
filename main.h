#include "PdhQuery.h"


#ifndef WIN32
#define TRUE 1
#define FALSE 1
#else
/*#define EWOULDBLOCK WSAEWOULDBLOCK*/
#endif

#define BUNKFD 32767

#define READSIZE 512

#define MAXLOGSIZE 9999999

#define DEBUGFILE "pdhstats.log"
/* if DEBUG is defined, then a log file will be written */


// name of the executable
#define SZAPPNAME            "pdhstats"
// internal name of the service
#define SZSERVICENAME        "perf_cnt_svc"
// displayed name of the service
#define SZSERVICEDISPLAYNAME "Performance Counter Service"
// list of service dependencies - "dep1\0dep2\0\0"
#define SZDEPENDENCIES       ""

void main_loop(unsigned int);
int initialize(void);
void my_perror(char *);
void process(char *, int);
void cprintf(int, const char *, ...);
void AddToMessageLog(char *, char *);
void log(const char *, ...);
void go(void);
void parse_command_line(int, char **);
void cleanup(void);
void disconnect(int);
void ErrorDisp(char *, DWORD, int);
void enumerate(const char *);

extern int port;
extern PdhQuery pq;
extern bool service;
extern DWORD dwErr;
extern bool servicestop;

extern bool DEBUG;
