/*
     Written By    :  Paul Maker
     Date          :   23/10/2001
     File          :   PdhQuery.h
     Description   :   Generically wraps the PDH stuff in windows NT
*/

#ifndef __PM_PDH_QUERY__
#define __PM_PDH_QUERY__

#include "pdh.h"  
#include "pdhmsg.h"
#include <windows.h>
#include <windef.h>
#include <time.h>

#define MAX_RAW_VALUES                    1

class PdhQuery
{
protected:
     /* user defined struct that the PDH calls use, i define it local to the class
     to avoid any undue namespace pollution*/
     typedef struct _tag_PDHCounterStruct 
     {
          HCOUNTER hCounter;      
          int nNextIndex;         
          int nOldestIndex;       
          int nRawCount;          
          PDH_RAW_COUNTER a_RawValue[MAX_RAW_VALUES]; 
     }PDHCOUNTERSTRUCT, *PPDHCOUNTERSTRUCT;

     /* query handle */
     HQUERY query_handle;

     /* list of names and counters */
     long count;
     char **names;
     PDHCOUNTERSTRUCT *counters;

     char last_error_str[100];

     bool upSizeNames(char *name);
     bool upSizeCounters();
     bool downSizeNames();
     bool downSizeCounters();

    
public:
     PdhQuery();
     ~PdhQuery();
     
     time_t last_update;
     bool counter_added;
     /* sets the object up */
     bool init();

     /* adds a counter, pass it the fully qualified counter name */
     bool addCounter(char *);
     bool removeCounter(char *);

     /* update the counters */
     bool updateCounters();
	 
     /* gets the value of a counter */
     unsigned long long getCounter(char *);
  
     /* dont free the pointer returned, its managed in the class */
     char *getLastErrorString();
	 int lookUpIndex(const char *);
     int last_error;	 

};

#endif /* __PM_PDH_QUERY__ */

