/*
     Written By    :  Paul Maker
     Date          :   23/10/2001
     File          :   PdhQuery.h
     Description   :   Generically wraps the PDH stuff in windows NT
*/

#ifndef __PM_PDH_QUERY__
#define __PM_PDH_QUERY__

#include <pdh.h>  
#include <pdhmsg.h>
#include <windows.h>

#define MAX_RAW_VALUES                    1//20

class PdhQuery
{
protected:
     /* user defined struct that the PDH calls use, i define it local to the class
     to avoid any undue namespace pollution*/
     typedef struct _tag_PDHCounterStruct 
     {
          HCOUNTER hCounter;      
          int nNextIndex;         
          int nOldestIndex;       
          int nRawCount;          
          PDH_RAW_COUNTER a_RawValue[MAX_RAW_VALUES]; 
     }PDHCOUNTERSTRUCT, *PPDHCOUNTERSTRUCT;

     /* query handle */
     HQUERY query_handle;

     /* list of names and counters */
     int count;
     char **names;
     PDHCOUNTERSTRUCT *counters;

     int last_error;
     char last_error_str[100];

     bool upSizeNames(char *name);
     bool upSizeCounters();
     bool downSizeNames();
     bool downSizeCounters();

     int lookUpIndex(char *name);

public:
     PdhQuery();
     ~PdhQuery();

     /* sets the object up */
     bool init();

     /* adds a counter, pass it the fully qualified counter name */
     bool addCounter(char *name);

     /* update the counters */
     bool updateCounters();

     /* gets the value of a counter */
     long getCounter(char *name);

     /* dont free the pointer returned, its managed in the class */
     char *getLastErrorString();
};

#endif /* __PM_PDH_QUERY__ */

/*
     Written By     :     Paul Maker
     Date          :     29/10/2001
     File          :     PdhQuery.cpp     
     Description     :     Wraps the NT Performance Data Helper
*/

#include "stdafx.h"

#include <malloc.h>
#include <string.h>

#include "PdhQuery.h"

/* zero the object */
PdhQuery::PdhQuery()
{
     count = 0;
     names = 0;
     counters = 0;
     last_error = 0;
}

/* close the object down */
PdhQuery::~PdhQuery()
{
     /* free the names */
     for(int i=0;i<count;i++) 
     {
          if(names[i]) free(names[i]);
     }
     if(names)free(names);
     /* free the counter array, this is not a pointer to pointer just a straight
     array */
     if(counters)free(counters);
     /* close the query handle */
     PdhCloseQuery(query_handle);
}

int PdhQuery::lookUpIndex(char *name)
{
     int retval = -1;
     for(int i=0;i<count;i++)
     {
          if(names[i]) /* care */
          {
               if(!stricmp(names[i],name))
               {
                    retval = i;
                    break;
               }
          }
     } 
     return retval;
}

bool PdhQuery::upSizeNames(char *name)
{
     bool retval = false;
     if(names = (char**)realloc(names,((count + 1) * sizeof(char*))))
     {
          names[count] = NULL;
          if(names[count] = strdup(name)) retval = true;
          else 
          {
               downSizeNames();
               last_error = PDH_MEMORY_ALLOCATION_FAILURE; 
          }
     }
     else last_error = PDH_MEMORY_ALLOCATION_FAILURE;
     return retval;
}

bool PdhQuery::upSizeCounters()
{
     bool retval = false;
     if(counters = (PDHCOUNTERSTRUCT*)realloc(counters,
                                                        ((count + 1) * sizeof(PDHCOUNTERSTRUCT))))
     {
          retval = true;
     }
     else last_error = PDH_MEMORY_ALLOCATION_FAILURE;
     return retval;
}

bool PdhQuery::downSizeNames()
{
     bool retval = true;
     if(names[count]) free(names[count]);
     names = (char**)realloc(names,(count * sizeof(char*)));
     return retval;
}

bool PdhQuery::downSizeCounters()
{
     bool retval = true;
     counters = (PDHCOUNTERSTRUCT*)realloc(counters,
                                                        (count * sizeof(PDHCOUNTERSTRUCT)));
     return retval;
}

/* init the object */
bool PdhQuery::init()
{
     bool retval = false;
     if((last_error = PdhOpenQuery(NULL,1,&query_handle)) == ERROR_SUCCESS)
     {
          retval = true;
     }
     return retval;
}

/* add a counter */
bool PdhQuery::addCounter(char *name)
{
     bool retval = false;

     if(upSizeCounters())
     {
          if((last_error = PdhAddCounter(query_handle,
                                                name,
                                               (DWORD)&counters[count], 
                                               &counters[count].hCounter)) == ERROR_SUCCESS) 
          {
               if(upSizeNames(name))
               {
                    ++count;
                    retval = true;
               }
               else 
               {
                    PdhRemoveCounter(counters[count].hCounter);
                    downSizeCounters();
               }
          }
          else downSizeCounters();
     }
     return retval;
}

bool PdhQuery::updateCounters()
{
     bool retval = false;
     if((last_error = PdhCollectQueryData(query_handle)) == ERROR_SUCCESS) retval = true;
     return retval;
}

long PdhQuery::getCounter(char *name)
{
     int i =0;
     long retval = -1;
     if((i = lookUpIndex(name)) > -1)
     {
          PDH_FMT_COUNTERVALUE pdhFormattedValue;     
          if((last_error = PdhGetFormattedCounterValue(counters[i].hCounter,
                                                PDH_FMT_LONG,
                                                NULL,
                                                &pdhFormattedValue)) == ERROR_SUCCESS)
          {
               retval = pdhFormattedValue.longValue;
          }
     }
     return retval;
}

char *PdhQuery::getLastErrorString()
{
     switch(last_error)
     {
     case ERROR_SUCCESS:
          strcpy(last_error_str,"There is no error");
          break;
     case PDH_INVALID_ARGUMENT:
          strcpy(last_error_str,"Invalid argument");
          break;
     case PDH_MEMORY_ALLOCATION_FAILURE:
          strcpy(last_error_str,"Memory allocation failure");
          break;
     case PDH_CSTATUS_BAD_COUNTERNAME: 
          strcpy(last_error_str,"The counter name path string could not be parsed "
                                     "or interpreted");
          break;
     case PDH_CSTATUS_NO_COUNTER:
          strcpy(last_error_str,"The specified counter was not found");
          break;
     case PDH_CSTATUS_NO_COUNTERNAME:
          strcpy(last_error_str,"An empty counter name path string was passed in");
          break;
     case PDH_CSTATUS_NO_MACHINE:
          strcpy(last_error_str,"A machine entry could not be created");
          break;
     case PDH_CSTATUS_NO_OBJECT:
          strcpy(last_error_str,"The specified object could not be found");
          break;
     case PDH_FUNCTION_NOT_FOUND:
          strcpy(last_error_str,"The calculation function for this counter could "
                                     "not be determined");
          break;
     case PDH_INVALID_HANDLE:
          strcpy(last_error_str,"The query handle is not valid");
          break; 
     case PDH_NO_DATA:
          strcpy(last_error_str,"No data for that counter");
          break;
     }
     return last_error_str;
} 